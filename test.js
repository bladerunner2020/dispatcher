/**
 * Created by Bladerunner on 20/01/2018.
 */
var http = require('http');
var D = require('./index');

var dispatcher = D.createDispatcher();

http.createServer(function (req, res) {
    if (!dispatcher.execute(req, res)) res.end();
}).listen(8585);


function MyHandler() {
    this.SomeVlaue  = 42;
    var that = this;

    this.handler = function(cmd, callback) {
        if (cmd != 'cmd42') return;
        console.log('My handler ' + this.SomeVlaue);
        setTimeout(function(){
            that.SomeVlaue = that.SomeVlaue*2;
            console.log('My handler - finished. ' + that.SomeVlaue);

            if (typeof callback == 'function')
                callback(that.SomeVlaue);

        }, 5000);

        return true;
    };
}

myhandler = new MyHandler();



dispatcher.on('execute', function(){
    console.log('execute! = ' + arguments[0]);
    var req = arguments[0];
    if (req.url) console.log('url = ' + req.url);
});

dispatcher.on('unknown', function() {
   console.log('unknown');
});

dispatcher.addHandler(function(req, res) {
    if (!req) return;

    switch (req.url) {
        case '/test':
            res.writeHead(200, {'Content-Type': 'text/html '});
            res.write('<meta content="text/html; charset=UTF-8" http-equiv="Content-Type">');
            res.write('Привет Мир!', 'utf8');
            res.end();
            return true;
            break;
    }
});

dispatcher.addHandler(function(cmd, value) {
    switch (cmd) {
        case 'cmd1':
            console.log('Handler 1. value=' + value);
            return true;
            break;
    }

});

dispatcher.addHandler(function(cmd, value1, value2) {
    switch (cmd) {
        case 'cmd2':
            console.log('Handler 2. value1=' + value1 +', value2=' + value2);
            return true;
            break;
    }
});

dispatcher.addHandler(myhandler.handler, myhandler);

dispatcher.execute('cmd42', function(v) {
    console.log('v=' + v);
    console.log(this.id);
}.bind({id: 'test'}));



dispatcher.execute('cmd1', 8);
dispatcher.execute('cmd2', 1, 3);