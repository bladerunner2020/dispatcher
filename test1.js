var _Debug = console.log;

var http = require('http');
var D = require('./index');
var myDispatcher = D.createDispatcher();

function parseUri (str) {
    var	o   = parseUri.options,
        m   = o.parser[o.strictMode ? "strict" : "loose"].exec(str),
        uri = {},
        i   = 14;

    while (i--) uri[o.key[i]] = m[i] || "";

    uri[o.q.name] = {};
    uri[o.key[12]].replace(o.q.parser, function ($0, $1, $2) {
        if ($1) uri[o.q.name][$1] = $2;
    });

    return uri;
}

parseUri.options = {
    strictMode: false,
    key: ["source","protocol","authority","userInfo","user","password","host","port","relative","path","directory","file","query","anchor"],
    q:   {
        name:   "queryKey",
        parser: /(?:^|&)([^&=]*)=?([^&]*)/g
    },
    parser: {
        strict: /^(?:([^:\/?#]+):)?(?:\/\/((?:(([^:@]*)(?::([^:@]*))?)?@)?([^:\/?#]*)(?::(\d*))?))?((((?:[^?#\/]*\/)*)([^?#]*))(?:\?([^#]*))?(?:#(.*))?)/,
        loose:  /^(?:(?![^:@]+:[^:@\/]*@)([^:\/?#.]+):)?(?:\/\/)?((?:(([^:@]*)(?::([^:@]*))?)?@)?([^:\/?#]*)(?::(\d*))?)(((\/(?:[^?#](?![^?#\/]*\.[^?#\/.]+(?:[?#]|$)))*\/?)?([^?#\/]*))(?:\?([^#]*))?(?:#(.*))?)/
    }
};



function HttpGate(port, dispatcher) {
    this.dispatcher = dispatcher;
    var that = this;

    dispatcher.on('unknown', function(cmd, callback){
        var answer = {};
        var name = cmd.name ? cmd.name : 'unknown command';
        answer.Error = 'invalid command: ' + cmd.name;

        if (callback) callback(answer);
    });


    http.createServer(function(req, res){
        if (req.url.indexOf('favicon.ico') > 0) return;

        var url = parseUri(decodeURI(req.url));

        var cmd = url.queryKey ? url.queryKey : {};
        cmd.name = url.path.replace(/^\/|\/$/g, '');

        // if (url.queryKey) {
        //     cmd.firstDate = url.queryKey.firstDate;
        //     cmd.lastDate = url.queryKey.lastDate;
        //     cmd.email = url.queryKey.email;
        // }

        _Debug('http-request. name = ' + cmd.name + ', id = ' + cmd.id, 'http_gate');

        that.dispatcher.execute(cmd, function(data){
            var s = data ? data : 'undefined';
            if (typeof s !== 'string') {
                s = JSON.stringify(data);
            }

            _Debug('http-response: ' + s, 'http_gate');

            this.response.write(s);
            this.response.end();
        }.bind({response : res}));
    }).listen(+port);
}


myDispatcher.addHandler(function (cmd, cb) {
    console.log(cmd);
    if (cb) cb('ok: ' + cmd.name);
    return true;
});

new HttpGate(8181, myDispatcher);