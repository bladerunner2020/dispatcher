/**
 * Created by Bladerunner on 13/07/2018.
 */
if (typeof  _Debug === 'undefined') {
    var _Debug = console.log;
}

function parseUri (str) {
    var	o   = parseUri.options,
        m   = o.parser[o.strictMode ? "strict" : "loose"].exec(str),
        uri = {},
        i   = 14;

    while (i--) uri[o.key[i]] = m[i] || "";

    uri[o.q.name] = {};
    uri[o.key[12]].replace(o.q.parser, function ($0, $1, $2) {
        if ($1) uri[o.q.name][$1] = $2;
    });

    return uri;
}

parseUri.options = {
    strictMode: false,
    key: ["source","protocol","authority","userInfo","user","password","host","port","relative","path","directory","file","query","anchor"],
    q:   {
        name:   "queryKey",
        parser: /(?:^|&)([^&=]*)=?([^&]*)/g
    },
    parser: {
        strict: /^(?:([^:\/?#]+):)?(?:\/\/((?:(([^:@]*)(?::([^:@]*))?)?@)?([^:\/?#]*)(?::(\d*))?))?((((?:[^?#\/]*\/)*)([^?#]*))(?:\?([^#]*))?(?:#(.*))?)/,
        loose:  /^(?:(?![^:@]+:[^:@\/]*@)([^:\/?#.]+):)?(?:\/\/)?((?:(([^:@]*)(?::([^:@]*))?)?@)?([^:\/?#]*)(?::(\d*))?)(((\/(?:[^?#](?![^?#\/]*\.[^?#\/.]+(?:[?#]|$)))*\/?)?([^?#\/]*))(?:\?([^#]*))?(?:#(.*))?)/
    }
};



if (typeof MyClass === 'undefined') {
    var Dispatcher = require('../index').Dispatcher;
}

var HttpDispatcher = function HttpDispatcher() {

    var http = require('http');

    return function (port) {
        Dispatcher.call(this);

        var that = this;
        this.port = port;

        http.createServer(function(req, res){
            if (req.url.indexOf('favicon.ico') > 0) return;

            var url = parseUri(decodeURI(req.url));

            var cmd = url.queryKey ? url.queryKey : {};
            cmd.name = url.path.replace(/^\/|\/$/g, '');

            // if (url.queryKey) {
            //     cmd.firstDate = url.queryKey.firstDate;
            //     cmd.lastDate = url.queryKey.lastDate;
            //     cmd.email = url.queryKey.email;
            // }

            _Debug('http-request. name = ' + cmd.name + ', id = ' + cmd.id, 'http_gate');

            that.execute(cmd, function(data){
                var s = data ? data : 'undefined';
                if (typeof s !== 'string') {
                    s = JSON.stringify(data);
                }

                _Debug('http-response: ' + s, 'http_gate');

                this.response.write(s);
                this.response.end();
            }.bind({response : res}));
        }).listen(+port);
    }

}();

HttpDispatcher.prototype = Object.create(Dispatcher.prototype); // MUST be before all functions!!!
HttpDispatcher.prototype.constructor = HttpDispatcher;


var myDispatcher = new HttpDispatcher(8181);


myDispatcher.addHandler(function (cmd, cb) {
    console.log(cmd);
    if (cb) cb('ok: ' + cmd.name);
    return true;
});