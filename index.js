function Dispatcher() {
    this.handlers = [];
    this.callbacks = {};
}

Dispatcher.prototype.execute = function () {
    this.callOn('execute', arguments);

    for (var i = 0; i < this.handlers.length; i++) {
        var handler = this.handlers[i];
        if (handler && handler.apply(this, arguments)) return true;
    }
    
    var cb = this.callbacks['unknown'];
    if (cb) cb.apply(this, arguments);
};


Dispatcher.prototype.addHandler = function(handler, context) {
    if (context) handler = handler.bind(context);
    this.handlers.push(handler);
};

Dispatcher.prototype.on = function (event, callback) {
    this.callbacks[event] = callback;
};

Dispatcher.prototype.callOn = function(event, args) {
    var cb = this.callbacks[event];
    if (cb) cb.apply(this, args);
};


function createDispatcher() {
    return new Dispatcher();
}


if (typeof exports === 'object') {
    exports.createDispatcher = createDispatcher;
    exports.Dispatcher = Dispatcher;
}

if ((typeof IR === 'object') && (typeof module === 'object')) {
    module['dispatcher'] = {
        createDispatcher : createDispatcher,
        Dispatcher: Dispatcher
    };
}
